from ctypes import *
import time

PLATFORM_SUFFIX = "64" if sizeof(c_voidp) == 8 else ""
VERSION = 0x00010706
VERSION = 0x00010802
#VERSION = 0x00010705

error_enum = [
    'FMOD_OK',
    'FMOD_ERR_BADCOMMAND',
    'FMOD_ERR_CHANNEL_ALLOC',
    'FMOD_ERR_CHANNEL_STOLEN',
    'FMOD_ERR_DMA',
    'FMOD_ERR_DSP_CONNECTION',
    'FMOD_ERR_DSP_DONTPROCESS',
    'FMOD_ERR_DSP_FORMAT',
    'FMOD_ERR_DSP_INUSE',
    'FMOD_ERR_DSP_NOTFOUND',
    'FMOD_ERR_DSP_RESERVED',
    'FMOD_ERR_DSP_SILENCE',
    'FMOD_ERR_DSP_TYPE',
    'FMOD_ERR_FILE_BAD',
    'FMOD_ERR_FILE_COULDNOTSEEK',
    'FMOD_ERR_FILE_DISKEJECTED',
    'FMOD_ERR_FILE_EOF',
    'FMOD_ERR_FILE_ENDOFDATA',
    'FMOD_ERR_FILE_NOTFOUND',
    'FMOD_ERR_FORMAT',
    'FMOD_ERR_HEADER_MISMATCH',
    'FMOD_ERR_HTTP',
    'FMOD_ERR_HTTP_ACCESS',
    'FMOD_ERR_HTTP_PROXY_AUTH',
    'FMOD_ERR_HTTP_SERVER_ERROR',
    'FMOD_ERR_HTTP_TIMEOUT',
    'FMOD_ERR_INITIALIZATION',
    'FMOD_ERR_INITIALIZED',
    'FMOD_ERR_INTERNAL',
    'FMOD_ERR_INVALID_FLOAT',
    'FMOD_ERR_INVALID_HANDLE',
    'FMOD_ERR_INVALID_PARAM',
    'FMOD_ERR_INVALID_POSITION',
    'FMOD_ERR_INVALID_SPEAKER',
    'FMOD_ERR_INVALID_SYNCPOINT',
    'FMOD_ERR_INVALID_THREAD',
    'FMOD_ERR_INVALID_VECTOR',
    'FMOD_ERR_MAXAUDIBLE',
    'FMOD_ERR_MEMORY',
    'FMOD_ERR_MEMORY_CANTPOINT',
    'FMOD_ERR_NEEDS3D',
    'FMOD_ERR_NEEDSHARDWARE',
    'FMOD_ERR_NET_CONNECT',
    'FMOD_ERR_NET_SOCKET_ERROR',
    'FMOD_ERR_NET_URL',
    'FMOD_ERR_NET_WOULD_BLOCK',
    'FMOD_ERR_NOTREADY',
    'FMOD_ERR_OUTPUT_ALLOCATED',
    'FMOD_ERR_OUTPUT_CREATEBUFFER',
    'FMOD_ERR_OUTPUT_DRIVERCALL',
    'FMOD_ERR_OUTPUT_FORMAT',
    'FMOD_ERR_OUTPUT_INIT',
    'FMOD_ERR_OUTPUT_NODRIVERS',
    'FMOD_ERR_PLUGIN',
    'FMOD_ERR_PLUGIN_MISSING',
    'FMOD_ERR_PLUGIN_RESOURCE',
    'FMOD_ERR_PLUGIN_VERSION',
    'FMOD_ERR_RECORD',
    'FMOD_ERR_REVERB_CHANNELGROUP',
    'FMOD_ERR_REVERB_INSTANCE',
    'FMOD_ERR_SUBSOUNDS',
    'FMOD_ERR_SUBSOUND_ALLOCATED',
    'FMOD_ERR_SUBSOUND_CANTMOVE',
    'FMOD_ERR_TAGNOTFOUND',
    'FMOD_ERR_TOOMANYCHANNELS',
    'FMOD_ERR_TRUNCATED',
    'FMOD_ERR_UNIMPLEMENTED',
    'FMOD_ERR_UNINITIALIZED',
    'FMOD_ERR_UNSUPPORTED',
    'FMOD_ERR_VERSION',
    'FMOD_ERR_EVENT_ALREADY_LOADED',
    'FMOD_ERR_EVENT_LIVEUPDATE_BUSY',
    'FMOD_ERR_EVENT_LIVEUPDATE_MISMATCH',
    'FMOD_ERR_EVENT_LIVEUPDATE_TIMEOUT',
    'FMOD_ERR_EVENT_NOTFOUND',
    'FMOD_ERR_STUDIO_UNINITIALIZED',
    'FMOD_ERR_STUDIO_NOT_LOADED',
    'FMOD_ERR_INVALID_STRING',
    'FMOD_ERR_ALREADY_LOCKED',
    'FMOD_ERR_NOT_LOCKED',
    'FMOD_ERR_RECORD_DISCONNECTED',
    'FMOD_ERR_TOOMANYSAMPLES'
]

def check_result(r):
    if r != 0:
        msg = error_enum[r]
        print("ERROR: Got FMOD_RESULT {0}: {1}".format(r, msg))
        raise Exception("ERROR: Got FMOD_RESULT {0}: {1}".format(r, msg))

class FMODVector(Structure):
    _fields_ = [
        ('x', c_float),
        ('y', c_float),
        ('z', c_float),
    ]

class FMOD3DAttributes(Structure):
    _fields_ = [
        ('position', FMODVector),
        ('velocity', FMODVector),
        ('forward', FMODVector),
        ('up', FMODVector),
    ]

class FMODStudioParameterDescription(Structure):
    _fields_ = [
        ('name', c_char_p),
        ('index', c_int),
        ('minimum', c_float),
        ('maximum', c_float),
        ('defaultValue', c_float),
        ('type', c_int),
    ]

class EventDescription(Structure):
    def __init__(self, studio, event_description):
        self.studio = studio
        self.event_description = event_description

    def get_path(self):
        path = create_string_buffer(1024)
        retrieved = c_int()
        check_result(self.studio.FMOD_Studio_EventDescription_GetPath(self.event_description, path, 1024, byref(retrieved)))
        return path.value

    def get_parameter_count(self):
        count = c_int();
        check_result(self.studio.FMOD_Studio_EventDescription_GetParameterCount(self.event_description, byref(count)))
        return count.value

    def get_parameters(self):
        count = self.get_parameter_count()
        params = []
        for i in range(count):
            ret = FMODStudioParameterDescription()
            self.studio.FMOD_Studio_EventDescription_GetParameterByIndex(self.event_description, i, byref(ret))
            params.append(ret)
        return params

    def has_cue(self):
        if VERSION >= 0x00010802:
            p = c_bool();
            check_result(self.studio.FMOD_Studio_EventDescription_HasCue(self.event_description, byref(p)))
            return p.value
        else:
            return False

    def is_3d(self):
        p = c_bool();
        check_result(self.studio.FMOD_Studio_EventDescription_Is3D(self.event_description, byref(p)))
        return p.value

    def is_oneshot(self):
        p = c_bool();
        check_result(self.studio.FMOD_Studio_EventDescription_IsOneshot(self.event_description, byref(p)))
        return p.value

#    def is_snapshot(self):
#        p = c_bool();
#        check_result(self.studio.FMOD_Studio_EventDescription_IsSnapshot(self.event_description, byref(p)))
#        return p.value

    def is_stream(self):
        p = c_bool();
        check_result(self.studio.FMOD_Studio_EventDescription_IsStream(self.event_description, byref(p)))
        return p.value

    def get_length(self):
        p = c_bool();
        check_result(self.studio.FMOD_Studio_EventDescription_GetLength(self.event_description, byref(p)))
        return p.value

class Bank(object):
    def __init__(self, studio, bank):
        self.studio = studio
        self.bank = bank

    def get_event_count(self):
        count = c_int();
        check_result(self.studio.FMOD_Studio_Bank_GetEventCount(self.bank, byref(count)))
        return count.value

    def get_event_list(self):
        count = self.get_event_count()
        ret = (c_voidp * count)()
        ret_count = c_int()
        self.studio.FMOD_Studio_Bank_GetEventList(self.bank, ret, count, byref(ret_count))
        return [EventDescription(self.studio, ed) for ed in ret]

class StudioSystem(object):
    studio_dll = None
    studio_sys = None

    def __init__(self):
        self.banks = {}
        self.event_descs = {}
        self.studio_dll = WinDLL("fmodstudioL" + PLATFORM_SUFFIX)
        self.lowlevel_dll = WinDLL("fmodL" + PLATFORM_SUFFIX)
        check_result(self.lowlevel_dll.FMOD_Debug_Initialize(0x000000002, 1, 0, "log.txt".encode('ascii')))
        
        self.studio_sys = c_voidp()
        check_result(self.studio_dll.FMOD_Studio_System_Create(byref(self.studio_sys), VERSION))
        check_result(self.studio_dll.FMOD_Studio_System_Initialize(self.studio_sys, 256, 0, 0, c_voidp()))

    def load_banks(self, *args):
        for bankname in args:
            if type(bankname) is list:
                self.load_banks(*bankname)
            else:
                #print('Loading bank: {}'.format(bankname.encode('ascii')))
                bank = c_voidp()
                check_result(self.studio_dll.FMOD_Studio_System_LoadBankFile(self.studio_sys, bankname.encode('ascii'), 0, byref(bank)))
                self.banks[bankname] = bank

    def get_event(self, soundname):
        if not self.event_descs.has_key(soundname):
            event_desc = c_voidp()
            check_result(self.studio_dll.FMOD_Studio_System_GetEvent(self.studio_sys, soundname.encode('ascii'), byref(event_desc)))
        else:
            event_desc = self.event_descs[soundname]

        event_inst = c_voidp()
        check_result(self.studio_dll.FMOD_Studio_EventDescription_CreateInstance(event_desc, byref(event_inst)))
        return EventInstance(self.studio_dll, event_inst)

    def set_listener_attributes(self, attributes, listener=0):
        check_result(self.studio_dll.FMOD_Studio_System_SetListenerAttributes(self.studio_sys, listener, byref(attributes)))

    def update(self):
        check_result(self.studio_dll.FMOD_Studio_System_Update(self.studio_sys))

    def release(self):
        check_result(self.studio_dll.FMOD_Studio_System_Update(self.studio_sys))

class EventInstance(object):
    def __init__(self, studio, event_inst):
        self.studio = studio
        self.event_inst = event_inst

    def set_volume(self, volume):
        check_result(self.studio.FMOD_Studio_EventInstance_SetVolume(self.event_inst, c_float(volume)))

    def start(self):
        check_result(self.studio.FMOD_Studio_EventInstance_Start(self.event_inst))

    def stop(self, mode=0):
        check_result(self.studio.FMOD_Studio_EventInstance_Stop(self.event_inst, mode))

    def get_description(self):
        ret = c_voidp()
        check_result(self.studio.FMOD_Studio_EventInstance_GetDescription(self.event_inst, byref(ret)))
        return EventDescription(self.studio, ret)

    def set_3d_attributes(self, attributes):
        check_result(self.studio.FMOD_Studio_EventInstance_Set3DAttributes(self.event_inst, byref(attributes)))

    def get_playback_state(self):
        state = c_long()
        check_result(self.studio.FMOD_Studio_EventInstance_GetPlaybackState(self.event_inst, byref(state)))
        return state.value

    def release(self):
        check_result(self.studio.FMOD_Studio_EventInstance_Release(self.event_inst))
        self.event_inst = None

    def get_parameter(self, idx):
        value = c_float()
        if type(idx) is int:
            check_result(self.studio.FMOD_Studio_EventInstance_GetParameterValueByIndex(self.event_inst, idx, byref(value)))
        else:
            check_result(self.studio.FMOD_Studio_EventInstance_GetParameterValue(self.event_inst, c_char_p(idx), byref(value)))

    def set_parameter(self, idx, value):
        value = c_float(value)
        if type(idx) is int:
            check_result(self.studio.FMOD_Studio_EventInstance_SetParameterValueByIndex(self.event_inst, idx, value))
        else:
            check_result(self.studio.FMOD_Studio_EventInstance_SetParameterValue(self.event_inst, c_char_p(idx), value))

    def __getitem__(self, idx):
        return self.get_parameter(idx)

    def __setitem__(self, idx, value):
        return self.set_parameter(idx, float(value))

if __name__ == '__main__':
    s = StudioSystem()

    attr = FMOD3DAttributes(
        position=FMODVector(x=0, y=0, z=0),
        velocity=FMODVector(x=0, y=0, z=0),
        forward=FMODVector(x=0, y=0, z=1),
        up=FMODVector(x=0, y=1, z=0)
    )
    s.set_listener_attributes(attr)

    bank_files = [
        'fmod examples/Master Bank.bank',
        'fmod examples/Master Bank.strings.bank',
        'fmod examples/Character.bank',
        'fmod examples/Music.bank',
        'fmod examples/Surround_Ambience.bank',
        'fmod examples/UI_Menu.bank',
        'fmod examples/Vehicles.bank',
        'fmod examples/Weapons.bank',
    ]
    if VERSION >= 0x00010802:
        bank_files.append('fmod examples/Object Panning.bank')

    s.load_banks(bank_files)
    #e = s.get_event('event:/Character/Hand Foley/Doorknob')
    #e['Direction'] = 1
    e = s.get_event('event:/Character/Footsteps/Footsteps')
    e['Surface'] = 1.5

    for name, bank in s.banks.items():
        bank = Bank(s.studio_dll, bank)
        count = bank.get_event_count()
        print('Bank: {} - with {} events'.format(name, count))
        for event in bank.get_event_list():
            print('\tevent: {}'.format(event.get_path()))
            flags = []
            for c in (
                'has_cue',
                'is_3d',
                'is_oneshot',
                #'is_snapshot',
                'is_stream',
            ):
                if getattr(event, c)():
                    flags.append(c)
            if len(flags) > 0:
                print('\t\tflags: {}'.format(', '.join(flags)))
            if event.get_parameter_count() > 0:
                print('\t\tparameters:')
                for param in event.get_parameters():
                    print('\t\t\t{} (min={}, max={}, def={})'.format(
                        param.name,
                        param.minimum,
                        param.maximum,
                        param.defaultValue,
                    ))
         

    e_attr = FMOD3DAttributes()

    t = time.time()
    loops = 0
    duration = 3
    l = 0
    is_oneshot = e.get_description().is_oneshot()
    while l < loops:
        e.start()
        play_state = e.get_playback_state()
        while play_state != 2 and (not is_oneshot and (time.time() - t) < 3):
            x = 2.5-3*(time.time()-t)
            e_attr.position = FMODVector(x=0, y=0, z=2)
            e_attr.velocity = FMODVector(x=0, y=0, z=0)
            e_attr.forward = FMODVector(x=0, y=0, z=1)
            e_attr.up = FMODVector(x=0, y=1, z=0)
            e.set_3d_attributes(e_attr)
            #e.set_pitch(x)

            s.update()
            time.sleep(0.010)
            play_state = e.get_playback_state()
        #e['Direction'] = 0
        l += 1
    if not is_oneshot:
        e.stop()
    e.release()
