# MidiPlayground - MidiDungeon
# 
# Latest version available at: https://github.com/j3hyde/midiplayground
# 
# Copyright (c) 2015 Jeffrey Kyllo
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
# ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
# CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


import pypm, time, random, copy, argparse, sys, traceback
import re, math
import traceback

import fmod

try:
  import blessings
  import colorama
except:
  pass

# Concept of views
# There is one root view which may delegate to sub-views.
# E.g. a view for the top and side buttons with another for the grid.
# The controller could then swap out/activate other views for the grid

class View(object):
  '''Views basically listen to model changes and issue MIDI events to represent them.  Input is also taken from MIDI, interpreted a bit, and emitted as events by the View.'''
  def __init__(self, width=8, height=8, left=0, top=0):
    self.listeners = []
    self.width = width
    self.height = height
    self.left = left
    self.top = top

  def setitem(self, o, i, v):
    '''Listen for GridModel changes.'''
    pass

  def handle_input(self):
    pass

  def add_listener(self, listener):
    self.listeners.append(listener)

class UIDriver(object):
  '''The UIDriver is responsible for rendering out to whatever display device is selected.'''
  def close(self):
    pass

  def get(self):
    return []

  def set(self, col, row, value):
    pass

  def clear(self, col=None, row=None):
    pass

  def commit(self):
    pass

  def close(self):
    pass

class ConsoleDriver(UIDriver):
  '''This driver simply prints to the console.'''
  def __init__(self, width=8, height=8):
    self.width = width
    self.height = height
    self.clear()

    # Used to parse player input at the prompt.  Supports col/row or left,
    # right, up, down movement
    self.re = re.compile("(?P<col>[0-9]+)(\s*,\s*|\s+)(?P<row>[0-9]+)|(?P<dir>[lrud])")

    # Maps entity types to display characters
    self.entity_map = {
      Player: 'P',
      Door: 'D',
      HealthPickup: 'H',
      Wall: 'W',
      Fire: 'F',
    }

    # Maps input letters to movement directions
    self.direction_map = {
      'l': (-1, 0),
      'r': (1, 0),
      'u': (0, -1),
      'd': (0, 1),
    }

    self.ready = False

  def _get_entity_repr(self, e):
    m = self.entity_map
    t = type(e)
    #if t == Player:
    #  return e.value
    #else:
    for i in m.keys():
      if isinstance(e, i):
        r = m[i]
        if r is None:
          r = e.value
        return r

    if type(e) == int:
      if e == 0:
        return ' '
      else:
        return e

    return type(e).__name__[0]

  def get(self):
    i = raw_input("> ")
    m = self.re.match(i)

    # No match on the player input
    if m is None:
      #print("Bad input.")
      return []

    d = m.groupdict()

    direction = d.get('dir', None)

    if direction:
      relative = True
      d['col'], d['row'] = self.direction_map.get(direction, (0, 0))
    else:
      relative = False

    event = UIInputEvent(int(d['col']), int(d['row']), 127, relative)
    return [event]

  def set(self, col, row, value):
    self.frame_buffer[row][col] = value

  def clear(self, col=None, row=None):
    self.frame_buffer = [[0 for c in range(self.width)] for r in range(self.height)]

  def commit(self):
    if not self.ready:
      return

    print('\n'+'\n'.join(
      [' '.join([str(self._get_entity_repr(c)) for c in r]) for r in self.frame_buffer]
    ))

  def close(self):
    pass

class TerminalDriver(UIDriver):
  '''This driver uses blessings and colorama.'''
  def __init__(self, width=8, height=8):
    colorama.init()
    self.terminal = blessings.Terminal()
    self.terminal.enter_fullscreen()

    self.width = width
    self.height = height
    self.clear()

    # Used to parse player input at the prompt.  Supports col/row or left,
    # right, up, down movement
    self.re = re.compile("(?P<col>[0-9]+)(\s*,\s*|\s+)(?P<row>[0-9]+)|(?P<dir>[lrud])")

    # Maps entity types to display characters
    self.entity_map = {
      Player: 'P',
      Door: 'D',
      HealthPickup: 'H',
      Wall: 'W',
      Fire: 'F',
    }

    # Maps input letters to movement directions
    self.direction_map = {
      'l': (-1, 0),
      'r': (1, 0),
      'u': (0, -1),
      'd': (0, 1),
    }

    self.ready = False

  def _get_entity_repr(self, e):
    m = self.entity_map
    t = type(e)
    #if t == Player:
    #  return e.value
    #else:
    for i in m.keys():
      if isinstance(e, i):
        r = m[i]
        if r is None:
          r = e.value
        return r

    if type(e) == int:
      if e == 0:
        return ' '
      else:
        return e

    return type(e).__name__[0]

  def get(self):
    i = raw_input("> ")
    m = self.re.match(i)

    # No match on the player input
    if m is None:
      #print("Bad input.")
      return []

    d = m.groupdict()

    direction = d.get('dir', None)

    if direction:
      relative = True
      d['col'], d['row'] = self.direction_map.get(direction, (0, 0))
    else:
      relative = False

    event = UIInputEvent(int(d['col']), int(d['row']), 127, relative)
    return [event]

  def set(self, col, row, value):
    self.frame_buffer[row][col] = value

  def clear(self, col=None, row=None):
    self.frame_buffer = [[0 for c in range(self.width)] for r in range(self.height)]

  def commit(self):
    if not self.ready:
      return

    self.terminal.clear()

    print('\n'+'\n'.join(
      [' '.join([str(self._get_entity_repr(c)) for c in r]) for r in self.frame_buffer]
    ))

  def close(self):
    self.terminal.exit_fullscreen()

class UIInputEvent(object):
  '''Represents a UI event in "app space" meaning that coordinates are
  converted away from the raw device coordinates (i.e. in grid-space, not
  MIDI-space).  The value currently represents the value from the input device,
  however, until I define a suitable grid-space value domain.'''
  def __init__(self, col, row, value, relative=False):
    self.col = col
    self.row = row
    self.value = value
    self.relative = relative

class DebugDriver(UIDriver):
  '''Wraps another UIDriver and simply logs events as they happen.'''
  def __init__(self, inner):
    self.inner = inner

  def get(self):
    self.log('get')
    if self.inner:
      return self.inner.get()
    else:
      return []

  def set(self, col, row, value):
    self.log('set({0}, {1}, {2})'.format(col, row, value))
    if self.inner:
      return self.inner.set(col, row, value)

  def clear(self, col=None, row=None):
    self.log('clear({0}, {1})'.format(col, row))
    if self.inner:
      return self.inner.clear()

  def commit(self):
    self.log('commit')
    if self.inner:
      return self.inner.commit()

  def close(self):
    self.log('close')
    if self.inner:
      return self.inner.close()

  def log(self, message):
    print(message)

class MidiTweakDriver(object):
  def __init__(self, device_id):
    self.device = pypm.Input(device_id)

  def get(self):
    events = []
    while self.device.Poll():
      for e in self.device.Read(1):
        control, value = self.map_midi_to_ui(e)
        events.append((control, value))
    return events

  @classmethod
  def map_midi_to_ui(cls, note):
    '''Maps Game of Life coordinates to MIDI note index.
    
    >>> MidiDriver.map_midi_to_ui(85)
    (5, 5)
    >>> MidiDriver.map_midi_to_ui(84)
    (4, 5)
    >>> MidiDriver.map_midi_to_ui(80)
    (0, 5)
    >>> MidiDriver.map_midi_to_ui(64)
    (0, 4)
    >>> MidiDriver.map_midi_to_ui(65)
    (1, 4)
    '''
    cmd = note[0][0] & 0b11110000
    channel = note[0][0] & 0b1111
    control = note[0][1] & 0b01111111
    value = note[0][2] & 0b01111111

    if cmd == 0b10110000:
      return control - 21, value
    else:
      return None

class MidiDriver(UIDriver):
  '''Drives UI interactions with a MIDI device.  Currently implemented with the
  Novation Launchpad Mini grid controller.  Ideally more mappings would be
  supported.'''
  def __init__(self, in_device_id, out_device_id):
    print "Opening devices:"

    self.in_device = pypm.Input(in_device_id)
    print "\tin: {0}, {1}".format(in_device_id, self.in_device)

    self.out_device = pypm.Output(out_device_id)
    print "\tin: {0}, {1}".format(out_device_id, self.out_device)
    self.write_queue = []

  def _get_entity_repr(self, e):
    t = type(e)
    if hasattr(e, 'value'):
      return e.value
    elif type(e) == int:
      return e
    else:
      return 0

  @classmethod
  def list_devices(cls):
    '''Queries the MIDI API for available devices.'''
    ret = []
    for i in range(pypm.CountDevices()):
      ret.append(pypm.GetDeviceInfo(i))
    return ret

  def get(self):
    '''Gets any available UIInputEvents from the attached MIDI device.'''
    events = []
    while self.in_device.Poll():
      for e in self.in_device.Read(1):
        coords = self.map_midi_to_ui(e[0][1])
        events.append(UIInputEvent(coords[0], coords[1], e[0][2]))
    return events

  def set(self, col, row, velocity=127):
    '''Sets the value of a light in the MIDI device's grid.'''
    index = self.map_ui_to_midi(col, row)
    self.write_queue.append([[144, index, self._get_entity_repr(velocity), 0], pypm.Time()])

  def clear(self, col=None, row=None):
    '''Clears the value of a light in the MIDI device's grid.'''
    if col is None and row is None:
      t = pypm.Time()
      self.write_queue += [ [[144, index, 0, 0], t] for index in range(8*16) ]
    else:
      index = self.map_ui_to_midi(col, row)
      self.write_queue.append([[144, index, 0, 0], pypm.Time()])

  def commit(self):
    '''Writes out the MIDI commands set up in set() and clear().  This must be called for those methods to take any actual effect.'''
    while len(self.write_queue) > 0:
      s = min(len(self.write_queue), 1024)
      self.out_device.Write(self.write_queue[:s])
      del self.write_queue[:s]

  def close(self):
    self.in_device.Close()
    self.out_device.Close()

  @classmethod
  def map_ui_to_midi(cls, col, row):
    '''Maps Game of Life coordinates to MIDI note index.
    
    >>> MidiDriver.map_ui_to_midi(5, 5)
    85
    >>> MidiDriver.map_ui_to_midi(8, 0)
    8
    '''
    return row * 16 + col

  @classmethod
  def map_midi_to_ui(cls, note):
    '''Maps Game of Life coordinates to MIDI note index.
    
    >>> MidiDriver.map_midi_to_ui(85)
    (5, 5)
    >>> MidiDriver.map_midi_to_ui(84)
    (4, 5)
    >>> MidiDriver.map_midi_to_ui(80)
    (0, 5)
    >>> MidiDriver.map_midi_to_ui(64)
    (0, 4)
    >>> MidiDriver.map_midi_to_ui(65)
    (1, 4)
    '''
    return (note % 16, int(note / 16))


#class MidiView(View):
#  def __init__(self, *args, **kwargs):
#    '''Takes a MIDI device to be used for displaying life.'''
#    if kwargs.has_key('ui_driver'):
#      ui_driver = kwargs.pop('ui_driver', None)
#    elif len(args) > 0:
#      ui_driver = args[0]
#      args = args[1:]
#    self.ui_driver = ui_driver
#
#    super(MidiView, self).__init__(*args, **kwargs)
#
#  def setitem(self, o, i, v):
#    if type(i) is str and i == 'paused':
#      self.ui_driver.set(8, 0, 127 if bool(v) else 1)
#    elif type(i) is tuple:
#      self.ui_driver.set(i[0], i[1], v)
#
#    self.ui_driver.commit()
#
#  def handle_input(self):
#    for event in self.ui_driver.get():
#      for listener in self.listeners:
#        listener(self, event)

class GridView(View):
    def __init__(self, ui_driver):
        super(GridView, self).__init__()
        self.ui_driver = ui_driver

    def setitem(self, o, i, v):
      self.ui_driver.set(i[0], i[1], v)

    def handle_input(self):
      for event in self.ui_driver.get():
        for listener in self.listeners:
          listener(self, event)

class GridModel(object):
  def __init__(self, width, height, data=None, default=None):
    '''
    >>> m = GridModel(5, 5)
    >>> m[0, 0]
    >>> m = GridModel(5, 5, ( \
        (1, 0, 0, 0, 0), \
        (0, 0, 0, 0, 0), \
        (1, 0, 0, 0, 0), \
        (0, 0, 0, 0, 0), \
        (0, 0, 0, 0, 0)))
    >>> m[0, 0]
    1
    >>> m[1, 0]
    0
    >>> m[0, 2]
    1
    '''
    self.width = width
    self.height = height
    self.default = default
    if data is None:
      self.model = [[default for c in range(width)] for r in range(height)]
    else:
      self.model = [[data[r][c] for c in range(width)] for r in range(height)]

  def __getitem__(self, i):
    '''Enables two-dimensional access to the model.  The first index is the column, the second is the row.
    
    >>> m = GridModel(5, 5)
    >>> m[2, 2]
    >>> m[4, 0] = 1
    >>> m[-1, 0]
    1
    >>> m[-10, 0]
    Traceback (most recent call last):
        ...
    IndexError: Index out of range.
    >>> m[5]
    Traceback (most recent call last):
        ...
    TypeError: Expected a 2-tuple but got a <type 'int'> instead.
    >>> m[2, 2, 2]
    Traceback (most recent call last):
        ...
    TypeError: Expected a 2-tuple but got a 3-tuple instead.
    >>> m[1:2,2]
    Traceback (most recent call last):
        ...
    ValueError: Only int-value indices are supported.
    >>> m[5, 5]
    Traceback (most recent call last):
        ...
    IndexError: Index out of range.
    >>> m[4, 5]
    Traceback (most recent call last):
        ...
    IndexError: Index out of range.
    >>> m[5, 4]
    Traceback (most recent call last):
        ...
    IndexError: Index out of range.

    '''
    if not type(i) is tuple:
      raise TypeError("Expected a 2-tuple but got a {0} instead.".format(type(i)))

    if not len(i) is 2:
      raise TypeError("Expected a 2-tuple but got a {0}-tuple instead.".format(len(i)))

    col = i[0]
    row = i[1]

    if type(col) != int or type(row) != int:
      raise ValueError("Only int-value indices are supported.")

    if col < 0:
      col += self.width
    if row < 0:
      row += self.height

    if col < 0 or col >= self.width or row < 0 or row >= self.height:
      raise IndexError("Index out of range.")

    return self.model[row][col]
        
  def __setitem__(self, i, value):
    '''Enables two-dimensional access to the model.  The first index is the column, the second is the row.
    
    >>> m = GridModel(5, 5)
    >>> m[2, 2] = 1
    >>> m[2, 2]
    1
    >>> m[4, 0]
    >>> m[-1, 0] = 1
    >>> m[4, 0]
    1
    >>> m[-10, 0] = 1
    Traceback (most recent call last):
        ...
    IndexError: Index out of range.
    >>> m[5] = 1
    Traceback (most recent call last):
        ...
    TypeError: Expected a 2-tuple but got a <type 'int'> instead.
    >>> m[2, 2, 2] = 1
    Traceback (most recent call last):
        ...
    TypeError: Expected a 2-tuple but got a 3-tuple instead.
    >>> m[1:2,2] = 1
    Traceback (most recent call last):
        ...
    ValueError: Only int-value indices are supported.
    >>> m[5, 5] = 1
    Traceback (most recent call last):
        ...
    IndexError: Index out of range.
    >>> m[4, 5] = 1
    Traceback (most recent call last):
        ...
    IndexError: Index out of range.
    >>> m[5, 4] = 1
    Traceback (most recent call last):
        ...
    IndexError: Index out of range.
    '''
    if type(i) is str:
      if i == 'paused':
        self._paused = bool(value)
      else:
        print 'got unexpected key: {0}'.format(i)
      return

    if not type(i) is tuple:
      raise TypeError("Expected a 2-tuple but got a {0} instead.".format(type(i)))

    if not len(i) is 2:
      raise TypeError("Expected a 2-tuple but got a {0}-tuple instead.".format(len(i)))

    col = i[0]
    row = i[1]

    if type(col) != int or type(row) != int:
      raise ValueError("Only int-value indices are supported.")

    if col < 0:
      col += self.width
    if row < 0:
      row += self.height

    if col < 0 or col >= self.width or row < 0 or row >= self.height:
      raise IndexError("Index out of range.")

    self.model[row][col] = value

  def __eq__(self, other):
    '''Checks that each element of two GridModels is equal.

    >>> m1 = GridModel(5, 5)
    >>> m2 = GridModel(4, 4)
    >>> m1 == m2
    False
    >>> m2 = GridModel(5, 5)
    >>> m1 == m2
    True
    >>> m1 = GridModel(5, 5, ( \
        (1, 1, 0, 0, 0), \
        (0, 0, 1, 1, 1), \
        (0, 0, 0, 0, 1), \
        (0, 0, 0, 0, 1), \
        (0, 0, 0, 1, 1)))
    >>> m2 = GridModel(5, 5, ( \
        (0, 1, 0, 0, 0), \
        (0, 0, 1, 1, 1), \
        (0, 0, 0, 0, 1), \
        (0, 0, 0, 0, 1), \
        (0, 0, 0, 1, 1)))
    >>> m2[0, 0] = 1
    >>> m1 == m2
    True
    '''
    if self.width != other.width or self.height != other.height:
      return False

    for c in range(self.width):
      for r in range(self.height):
        if self[c, r] != other[c, r]:
          return False
    return True

  def animate(self, t):
    pass

  def tick(self):
    pass

  def __str__(self):
    return "({0})".format(",\n ".join(["({0})".format(", ".join([str(c) for c in row])) for row in self.model] ))


class ItemBindingMixin(object):
  def __init__(self, *args, **kwargs):
    super(ItemBindingMixin, self).__init__(*args, **kwargs)
    self._binding_listeners = []

  def add_listener(self, listener):
    '''Addes a callable to the list of listeners.  The callable should take three arguments: the object emitting the message, the __setitem__ index, and the new value.'''
    self._binding_listeners.append(listener)

  def __setitem__(self, i, value):
    super(ItemBindingMixin, self).__setitem__(i, value)
    for listener in self._binding_listeners:
      listener(self, i, value)

class BoundGridModel(ItemBindingMixin, GridModel):
  pass

class ListenerMixin(object):
  def __init__(self, *args, **kwargs):
    super(ListenerMixin, self).__init__(*args, **kwargs)
    self._binding_listeners = []

  def add_listener(self, listener):
    '''Addes a callable to the list of listeners.  The callable should take three arguments: the object emitting the message, the __setitem__ index, and the new value.'''
    self._binding_listeners.append(listener)

  def remove_listener(self, listener):
    self._binding_listeners.remove(listener)

  def notify(self, *args, **kwargs):
    #print('Notify: {}'.format(self))
    for listener in self._binding_listeners:
      listener(self, *args, **kwargs)

class Room(ListenerMixin):
  def __init__(self, *args, **kwargs):
    super(Room, self).__init__(*args, **kwargs)
    self._entities = set()
    self._old_positions = {}

  def add_entity(self, entity):
    self._entities.add(entity)
    self._old_positions[entity] = entity.position

    self.notify(entity.position, entity)

  def remove_entity(self, entity):
    self._entities.remove(entity)
    del self._old_positions[entity]

    # This should notify with either a 0 value or that of another entity
    self.notify(entity.position, 0)

  def entity_updated(self, entity):
    old_pos = self._old_positions[entity]
    next_entity = self[old_pos]
    if next_entity is None:
      self.notify(old_pos, 0)
    else:
      self.notify(next_entity.position, next_entity)

    self._old_positions[entity] = entity.position
    self.notify(entity.position, entity)

  def update_all(self):
    for c in range(8):
      for r in range(8):
        e = self[c, r]
        if e is None:
          self.notify((c, r), 0)
        else:
          self.notify(e.position, e)

    #for entity in self._entities:
    #  self.notify(entity.position, entity)

  def tick(self):
    for e in self._entities:
      if hasattr(e, 'tick') and callable(e.tick):
        e.tick()

  def animate(self, t):
    for e in self._entities:
      if hasattr(e, 'animate') and callable(e.animate):
        e.animate(t)

  def get_by_type(self, entity_type):
    # Could be generalised to taking kwargs as constraints
    for e in self._entities:
      if isinstance(e, entity_type):
        return e

    return None

  def get_all_by_type(self, entity_type):
    # Could be generalised to taking kwargs as constraints
    entities = []
    for e in self._entities:
      if isinstance(e, entity_type):
        entities.append(e)

    return entities

  def __getitem__(self, i):
    self.test_in_room(i)

    for e in self._entities:
      if e.position == i:
        return e

    return None

  def __setitem__(self, i, v):
    self.test_in_room(i)

    del self[i]
    self._entities.add(v)
    v.parent = self
    v.position = i

  def __delitem__(self, i):
    self.test_in_room(i)

    e = self[i]
    if e is not None:
      e.parent = None

  def test_in_room(self, i):
    if not type(i) is tuple:
      raise TypeError("Expected a 2-tuple but got a {0} instead.".format(type(i)))

    if not len(i) is 2:
      raise TypeError("Expected a 2-tuple but got a {0}-tuple instead.".format(len(i)))

    if i[0] not in range(8) or i[1] not in range(8):
      raise ValueError("Room coordinates must be within range [0,0] to [7,7]")

    return True


class Entity(object):
  _parent = None
  _position = (0, 0)
  _value = 0
  _enabled = True
  _active = True
  blocking = True
  sound_loop = None

  @property
  def enabled(self):
    return self._enabled

  @enabled.setter
  def enabled(self, value):
    self._enabled = value

  @property
  def active(self):
    return self._active

  @active.setter
  def active(self, value):
    '''Manages the active state of the entity.

    >>> e = Entity()
    >>> e.on_activate = lambda x: print('activate')
    >>> e.on_deactivate = lambda x: print('deactivate')
    >>> e.active
    False
    >>> e.active = False
    deactivate
    >>> e.active
    False
    >>> e.active = False
    >>> e.active = True
    activate
    >>> e.active = True

    '''
    value = bool(value)
    if value and not self._active:
      self.on_activate()
    elif not value and self._active:
      self.on_deactivate()

    self._active = value

  def on_activate(self):
    if self.sound_loop:
      NotificationCenter.get_instance().emit('play_sound', self.sound_loop, unique=self)

  def on_deactivate(self):
    if self.sound_loop:
      NotificationCenter.get_instance().emit('stop_sound', self.sound_loop, unique=self)

  def __init__(self, position=(0, 0), parent=None):
    #print('Entity init: {}'.format(self))
    self.position = position
    self.parent = parent

  def tick(self):
    '''This means to take a turn.'''
    pass

  def animate(self, t):
    '''This means to animate a frame.'''
    pass

  def is_adjacent(self, other):
    position = other
    if isinstance(other, Entity):
      position = other.position

    if distance(self.position, position) <= 1:
      return True
    else:
      return False

  @property
  def position(self):
    return self._position

  @position.setter
  def position(self, value):
    self._position = value
    self.notify()

  @property
  def value(self):
    return self._value

  @value.setter
  def value(self, value):
    self._value = value
    self.notify()

  @property
  def parent(self):
    return self._parent

  @parent.setter
  def parent(self, value):
    #print('Set parent: {} - {}'.format(self, value))
    if self._parent is not None:
      self._parent.remove_entity(self)

    self._parent = value

    if self._parent is not None:
      self._parent.add_entity(self)

  def notify(self):
    if self.parent is not None:
      self.parent.entity_updated(self)

class CombatMixin(object):
  damage = 0
  shield = 0
  _health = 0
  max_health = 100
  min_health = 0

  @property
  def health(self):
    return self._health

  @health.setter
  def health(self, value):
    self._health = min(self.max_health, max(self.min_health, value))

  def apply_damage(self, amount, other=None):
    # Could be extended with damage types, etc.

    if hasattr(self, 'shield'):
      amount = max(0, amount - self.shield)
    if hasattr(self, 'health'):
      self.health -= amount

    if self.health <= 0:
      if hasattr(self, 'die'):
        self.die(other)

class Player(CombatMixin, Entity):
  _health = 50
  # could make damage a property calculated from powerups, etc.
  damage = 10
  shield = 0
  _value = 127

  #@property
  #def value(self):
  #  return self._value

  def interact(self, position, entity):
    if distance(self.position, position) > 1:
      return

    if isinstance(entity, Pickup):
      entity.apply(self)
    elif isinstance(entity, Monster) and isinstance(entity, CombatMixin):
      entity.apply_damage(self.damage)

    if entity is None or not entity.blocking:
      self.position = position

    if entity is None or not entity.blocking or isinstance(entity, Door):
      NotificationCenter.get_instance().emit('play_sound', 'event:/Character/Footsteps/Footsteps', parameters=dict(Surface=3), duration=0.5, unique=self, at_entity=self)

      if isinstance(entity, Door):
          NotificationCenter.get_instance().emit('play_sound', 'event:/Character/Hand Foley/Doorknob', parameters=dict(Direction=1), at_entity=entity)

  def die(self, other):
    NotificationCenter.get_instance().emit('player_died', self, other)

class Pickup(Entity):
  blocking = False
  _value = 127

  #@property
  #def value(self):
  #  return self._value

  def apply(self, player):
    pass

class Door(Pickup):
  # Door is blocking because the 'door' notification handler moves the player separately
  blocking = True
  _value = 50

  def __init__(self, next_room, *args, **kwargs):
    super(Door, self).__init__(*args, **kwargs)
    self.next_room = next_room

  def apply(self, player):
    NotificationCenter.get_instance().emit('door', self)

  #@property
  #def value(self):
  #  return 50

class HealthPickup(Pickup):
  def apply(self, player):
    player.health += 10
    self.parent.remove_entity(self)
    
    return True

class Wall(Entity):
  blocking = True
  _value = 25

  #@property
  #def value(self):
  #  return _value

class Fire(Pickup):
  _value = 100
  sound_loop = 'event:/Environment/Fire'

  min_value = 50
  max_value = 100
  period = 5

#  values = [
#     0,  1,  2,  3,
#    16, 17, 18, 19,
#    32, 33, 34, 35,
#    48, 49, 50, 51,
#  ]

  values = [
    1, 2, 3,
    18, 19,
    34, 35,
    50, 51,
  ]

  #@property
  #def value(self):
  #  return self._value

  def animate(self, t):
    x = int(t % self.period / float(self.period) * len(self.values))
    self._value = self.values[x]
    self.notify()

  def apply(self, player):
    if isinstance(player, CombatMixin):
      player.apply_damage(5, self)
    else:
      player.health -= 5

    return True

class Monster(Entity):
  _value = 10

  #@property
  #def value(self):
  #  return self._value

  def die(self, other):
    print('Player killed the {}'.format(type(self).__name__))
    self.parent = None

class StationaryMonster(CombatMixin, Monster):
  blocking = True

  def __init__(self, damage=10, shield=5, *args, **kwargs):
    super(StationaryMonster, self).__init__(*args, **kwargs)
    self.damage = damage
    self.shield = shield

  def tick(self):
    if self.parent is None:
      return

    player = self.parent.get_by_type(Player)
    if player is None:
      # Player isn't in this room
      return

    if player.is_adjacent(self):
      player.apply_damage(self.damage, self)

class SoundDaemon(object):
  def __init__(self, nc=None, listener=None):
    self.sounds = {}
    self.tracking = {}
    self.listener = listener

    if nc is None:
      nc = NotificationCenter.get_instance()
    nc.subscribe('play_sound', self.play_sound)

    self.system = fmod.StudioSystem()

  def load_banks(self, *args):
    self.system.load_banks(*args)

  def get_sound(self, name, unique=None):
    key = (name, unique)
    if unique is not None:
      if self.sounds.has_key(key):
        snd = self.sounds[key]
      else:
        snd = self.system.get_event(name)
        self.sounds[key] = snd
    else:
      snd = self.system.get_event(name)

    return snd

  def play_sound(self, notification, name, unique=None, at_entity=None, parameters={}, duration=None):
    snd = self.get_sound(name, unique)
    for k, v in parameters.items():
      snd[k] = v

    if snd.get_playback_state() != 0:
      snd.start()

    if duration:
      t = time.time() + duration
    else:
      t = None

    self.tracking[snd] = (t, at_entity)

  def set_listener(self, entity):
    self.listener = entity

  def update(self):
    t = time.time()
    for snd, track in self.tracking.items():
      limit, at_entity = track
      if limit is not None and t > limit:
        snd.stop()

      if at_entity is not None:
        pos = fmod.FMODVector(x=at_entity.position[0], y=at_entity.position[1], z=-1)
        attr = fmod.FMOD3DAttributes(position=pos, up=fmod.FMODVector(z=1), forward=fmod.FMODVector(x=1))
        snd.set_3d_attributes(attr)

    if self.listener:
      pos = fmod.FMODVector(x=self.listener.position[0], y=self.listener.position[1])
      attr = fmod.FMOD3DAttributes(position=pos, up=fmod.FMODVector(z=1), forward=fmod.FMODVector(x=1))
      self.system.set_listener_attributes(attr)

    self.system.update()

class NotificationCenter(object):
  instance = None

  @classmethod
  def get_instance(cls):
    if cls.instance is None:
      cls.instance = NotificationCenter()

    return cls.instance

  def __init__(self):
    self._listeners = {}

  def subscribe(self, name, callback):
    l = self._listeners.get(name, [])
    l.append(callback)
    self._listeners[name] = l

  def emit(self, name, *args, **kwargs):
    l = self._listeners.get(name, [])
    for listener in l:
      listener(name, *args, **kwargs)

class Game(object):
  _model = None

  def __init__(self, ui_driver, width=8, height=8):
    #self.model = BoundGridModel(width, height)
    #self.model.add_listener(GridView().setitem)
    #self.view = MidiView(ui_driver)

    self.notifier = NotificationCenter.get_instance()
    self.notifier.subscribe('door', self.door_happened)

    self.sound_daemon = SoundDaemon()
    self.sound_daemon.load_banks(
      'fmod examples/Master Bank.bank',
      'fmod examples/Master Bank.strings.bank',
      'fmod examples/Character.bank',
      'fmod examples/Music.bank',
      #'fmod examples/Object Panning.bank',
      'fmod examples/Surround_Ambience.bank',
      'fmod examples/UI_Menu.bank',
      'fmod examples/Vehicles.bank',
      'fmod examples/Weapons.bank',
    )

    self.ui_driver = ui_driver
    self.view = GridView(ui_driver)
    self.view.add_listener(self.input_handler)
    #self.model.add_listener(self.view.setitem)

    self.rooms = [Room(), Room()]
    for room in self.rooms:
      for i in range(8):
        Wall((0, i), room)
        Wall((7, i), room)
        Wall((i, 0), room)
        Wall((i, 7), room)

    HealthPickup((2, 2), self.rooms[1])
    HealthPickup((5, 2), self.rooms[1])
    HealthPickup((2, 5), self.rooms[1])
    HealthPickup((5, 5), self.rooms[1])
    self.rooms[1][3,7] = Door(0)

    self.model = self.rooms[0]

    self.model[4,0] = Door(1)
    self.model[3,7] = Door(1)
    self.model[0,4] = Door(1)
    self.model[7,3] = Door(1)
    self.model[3,6] = Fire()
    self.model[2,5] = StationaryMonster()

    self.player = Player(position=(4, 1), parent=self.model)
    self.sound_daemon.set_listener(self.player)

    HealthPickup((5,5), self.model)

    if config.tweak:
      d = find_device(config.tweak)
      self.tweak = MidiTweakDriver(d[0])
    else:
      self.tweak = None

  @property
  def model(self):
    return self._model

  @model.setter
  def model(self, value):
    if self._model:
      self._model.remove_listener(self.view.setitem)
    self._model = value
    self._model.add_listener(self.view.setitem)
    self._model.update_all()

  def door_happened(self, name, door):
    room_no = door.next_room
    self.model = self.rooms[room_no]
    self.player.parent = self.model

    p0 = door.position
    p1 = [0,0]

    for i in range(2):
      if p0[i] == 0:
        p1[i] = 7
      elif p0[i] == 7:
        p1[i] = 0
      elif p0[i] == 4:
        p1[i] = 3
      elif p0[i] == 3:
        p1[i] = 4

    self.player.position = tuple(p1)

  def player_died(self, player, killer):
    print("The Player died!")
    self.running = False

  def run(self, speed=1):
    self.running = True
    next_tick = time.time()
    while self.running:
      now = time.time()
      if now >= next_tick:
        next_tick = now + speed
        self.model.animate(now)

      try:
        #print('Health:', self.player.health)
        self.view.handle_input()
        self.model.tick()
      except EOFError:
        break

      tweak_map = [
        (Player, '_value'),
        (Monster, '_value'),
        (Wall, '_value'),
        (Door, '_value'),
        (Pickup, '_value'),
        (Fire, 'min_value'),
        (Fire, 'max_value'),
      ]

      if self.tweak:
        events = self.tweak.get()
        for control, value in events:
          if control >= 0 and control < len(tweak_map):
            value = value / 8
            value = value%4 + value/4*16

            setattr(tweak_map[control][0], tweak_map[control][1], value)
            for entity in self.model.get_all_by_type(tweak_map[control][0]):
              entity.notify()
              #print(control, value, Player._value)

      self.ui_driver.commit()
      self.sound_daemon.update()
      time.sleep(0.1)

  def input_handler(self, source, uievent):
    '''Receives input events from the view.'''
    if uievent.value == 0:
      return

    col = uievent.col
    row = uievent.row

    if uievent.relative:
      col += self.player.position[0]
      row += self.player.position[1]

    if row < 8 and col < 8 and row >= 0 and col >= 0:
      v = self.model[col, row]
      d = distance(self.player.position, (col, row))

      # if d is more than 1 then must interact multiple times along a path

      self.player.interact((col, row), v)

def find_device(dev_name):
  n = pypm.CountDevices()
  in_device = None
  out_device = None
  for i in range(n):
    info = pypm.GetDeviceInfo(i)
    if info[1] == dev_name and info[2] == True:
      in_device = i
    if info[1] == dev_name and info[3] == True:
      out_device = i
  return (in_device, out_device)

def distance(t0, t1):
  return math.sqrt((t1[0]-t0[0])**2 + (t1[1]-t0[1])**2)

def test():
  import unittest, doctest
  doctest.testmod()

def get_argparser():
  parser = argparse.ArgumentParser()
  parser.add_argument('--test', default=False, action='store_true')
  parser.add_argument('--list', action='store_true')
  parser.add_argument('--device')
  parser.add_argument('--tweak')
  parser.add_argument('--indevice', type=int)
  parser.add_argument('--outdevice', type=int)
  parser.add_argument('--verbose', '-v', action='store_true')
  parser.add_argument('--text', default=False, action='store_true')
  parser.add_argument('--terminal', default=False, action='store_true')
  return parser

def get_config():
  return get_argparser().parse_args()

def print_help():
  get_argparser().print_help()

def get_ui_driver(config):
  uidriver = None

  if config.text:
    uidriver = ConsoleDriver()
  elif config.terminal:
    uidriver = TerminalDriver()
  else:
    # assume MIDI Driver
    pypm.Initialize()

    if config.device:
      d = find_device(config.device)
    else:
      d = (config.indevice, config.outdevice)
    if not d is None or not (d[0] is None or d[1] is None):
#    try:
#      in_device = pypm.Input(d[0])
#    except:
#      print("Error: Could not open input device.")
#      pypm.Terminate()
#      return
#
#    try:
#      out_device = pypm.Output(d[1])
#    except:
#      print("Error: Could not open output device.")
#      pypm.Terminate()
#      return
      print d
      uidriver = MidiDriver(d[0], d[1])

  if config.verbose:
    uidriver = DebugDriver(uidriver)

  return uidriver

def main(config):
  uidriver = get_ui_driver(config)
  if uidriver is None:
    print 'No input/output device found.  Exiting.'
  else:
    #print("Set up i/o driver: {}".format(uidriver))
    pass

  try:
    time.sleep(2)
    app = Game(uidriver)
    uidriver.ready = True
    uidriver.commit()
    app.run(1)
  except Exception as e:
    traceback.print_exc()
  finally:
    uidriver.clear()
    uidriver.commit()
    uidriver.close()
    if not config.text and not config.terminal:
      pypm.Terminate()
    return


if __name__ == '__main__':
  config = get_config()

  if config.test:
    test()
  elif config.list:
    devs = MidiDriver.list_devices()
    i = 0
    for dev in devs:
      print('{0} (id: {1}, in: {2}, out: {3})'.format(dev[1], i, dev[2] == 1, dev[3] == 1))
      i += 1
  elif config.text or config.terminal or not config.device is None or not (config.indevice is None or config.outdevice is None):
    main(config)
  else:
    print_help()



# palette
#[md.set(x%4, x/4, x % 4 + x/4*16) for x in range(16)]
